var soap = require('soap')
var path = require('path')
var config = require(path.join(__dirname, '..', 'config', 'config'))
var Promise = require('promise')
require('promise/lib/rejection-tracking').enable()
var ldap = module.exports = {}

ldap.login = function (email, clave, options) {
  var promise = new Promise(function (resolve, reject) {
    var args = {email: email, clave: clave}
    var url = config.ldap['prod']
    if (options.dev) {
      url = config.ldap['dev']
    }
    soap.createClient(url, function (err, client) {
      if (err) reject(err)
      client.validar(args, function (err, result) {
        if (err) return reject(err)
        if (parseInt(result.return)) {
          return resolve(result)
        } else {
          return reject(err)
        }
      })
    })
  })
  return promise
}

ldap.buscarPorEMailMasDatos = function (email, options) {
  var promise = new Promise(function (resolve, reject) {
    var args = {email: email}
    var url = config.ldap['prod']
    if (options.dev) {
      url = config.ldap['dev']
    }
    soap.createClient(url, function (err, client) {
      if (err) return reject(err)
      client.buscarporemail_mas_datos(args, function (err, result) {
        if (err) return reject(err)
        return resolve(result)
      })
    })
  })
  return promise
}
