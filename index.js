var path = require('path')
var ldap = require(path.join(__dirname, 'services', 'ldap'))
var Promise = require('promise')
require('promise/lib/rejection-tracking').enable()

exports.ldap = function (email, clave, options) {
  var promise = new Promise(function (resolve, reject) {
    if (!email || !clave) {
      reject({ 'ERROR': 'Los campos email y clave no pueden estar vacíos.' })
    }

    ldap.login(email, clave, { dev: options.dev })
    .then((result) => {
      ldap.buscarPorEMailMasDatos(email, { dev: options.dev })
        .then((result) => {
          resolve(result.return)
        })
    })
    .catch(error => {
      if (error) console.log(error)
      reject({ 'ERROR': 'El email o la clave son incorrectas.' })
    })
  })

  return promise
}

exports.ldap.buscarPorEmailMasDatos = function(email, options) {
  var promise = new Promise(function (resolve, reject) {
    if (!email) {
      reject({ 'ERROR': 'El email no puede estar vacío.' })
    }

    ldap.buscarPorEMailMasDatos(email, { dev: options.dev })
    .then((result) => {
      resolve(result.return)
    })
    .catch(error => {
      if (error) console.log(error)
      reject({ 'ERROR': 'El email es incorrecto.' })
    })
  })

  return promise
}
